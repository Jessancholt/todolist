﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Models.Home
{
    public class TaskInfo
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Название задачи являтся обязательным полем.")]
        public string Title { get; set; }
    }
}
