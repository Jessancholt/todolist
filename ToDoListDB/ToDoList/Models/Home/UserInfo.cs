﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ToDoListDB.Model;

namespace ToDoList.Models.Home
{
    public class UserInfo
    {
        public int Id { get; set; }

        //[StringLength(10, MinimumLength = 1, ErrorMessage = "Length is invalid.")]
        [Required(ErrorMessage = "FirstName is required")]
        [Display(Name = "Имя: ")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия: ")]
        public string LastName { get; set; }
        [Display(Name = "Логин: ")]
        public string Login { get; set; }

        //[Compare("ConfirmPassword", ErrorMessage = null)]
        //[Display(Name = "Login: ")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль: ")]
        public string Password { get; set; }

        public bool IsDeleted { get; set; }
        public int Age { get; set; }

        [Display(Name = "Дата рождения: ")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Роль: ")]
        public string Role { get; set; }

        public int TaskCount { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public int[] SelectedTaskIds { get; set; }
    }
}
