﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Models
{
    public enum SortState
    {
        FirstNameAsc,    // по имени по возрастанию
        LastNameAsc,    // по имени по возрастанию
        FirstNameDesc,   // по фамилии по убыванию
        LastNameDesc,   // по фамилии по убыванию
        IdAsc, // по Id по возрастанию
        IdDesc, // по Id по убыванию
        TitleAsc,
        TitleDesc,
        DescriptionAsc,
        DescriptionDesc
    }
}
