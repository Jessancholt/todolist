﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToDoList.Models.ViewModels
{
    public class SortViewModel
    {
        public SortState IdSort { get; private set; } // значение для сортировки по Id
        public SortState FirstNameSort { get; private set; } // значение для сортировки по имени
        public SortState LastNameSort { get; private set; } // значение для сортировки по фамилии

        public SortState TitleSort { get; private set; } // значение для сортировки по фамилии
        public SortState DescriptionSort { get; private set; } // значение для сортировки по фамилии
        public SortState Current { get; private set; }     // текущее значение сортировки

        public SortViewModel(SortState sortOrder)
        {
            IdSort = sortOrder == SortState.IdAsc ? SortState.IdDesc : SortState.IdAsc;
            FirstNameSort = sortOrder == SortState.FirstNameAsc ? SortState.FirstNameDesc : SortState.FirstNameAsc;
            LastNameSort = sortOrder == SortState.LastNameAsc ? SortState.LastNameDesc : SortState.LastNameAsc;
            TitleSort = sortOrder == SortState.TitleAsc ? SortState.TitleDesc : SortState.TitleAsc;
            DescriptionSort = sortOrder == SortState.DescriptionAsc ? SortState.DescriptionDesc : SortState.DescriptionAsc;
            Current = sortOrder;
        }
    }
}
