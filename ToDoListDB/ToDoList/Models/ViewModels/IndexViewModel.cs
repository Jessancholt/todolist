﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Models.Home;

namespace ToDoList.Models.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<UserInfo> Users { get; set; }
        public IEnumerable<TaskInfo> Tasks { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public SortViewModel SortViewModel { get; set; }
    }
}
