﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Models;
using ToDoList.Models.Home;
using ToDoList.Models.ViewModels;
using ToDoListDB.Model;

namespace ToDoList.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ITaskService _taskService;

        public UserController(IUserService userService, ITaskService taskService)
        {
            _userService = userService;
            _taskService = taskService;
        }
        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var user = _userService.GetInfo(id);
            var model = new UserInfo
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Login = user.Login,
                Password = user.Password,
                Role = user.Role.Name,
                TaskCount = user.Tasks.Count,
                DateOfBirth = user.DateOfBirth,
                IsDeleted = user.IsDeleted,
                Tasks = user.Tasks.ToList()
            };

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult GetAll(string firstName, string lastName, int page = 1, SortState sortOrder = SortState.IdAsc)
        {
            int pageSize = 3;   // количество элементов на странице

            var users = _userService.GetAll().AsQueryable();

            var model = users
                .Select(u => new UserInfo
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    IsDeleted = u.IsDeleted
                });


            IQueryable<UserInfo> source = model;

            if (!String.IsNullOrEmpty(firstName))
            {
                source = source.Where(p => p.FirstName.Contains(firstName));
            }

            if (!String.IsNullOrEmpty(lastName))
            {
                source = source.Where(p => p.LastName.Contains(lastName));
            }

            // сортировка
            switch (sortOrder)
            {
                case SortState.IdDesc:
                    source = source.OrderByDescending(s => s.Id);
                    break;

                case SortState.FirstNameAsc:
                    source = source.OrderBy(s => s.FirstName);
                    break;

                case SortState.FirstNameDesc:
                    source = source.OrderByDescending(s => s.FirstName);
                    break;

                case SortState.LastNameAsc:
                    source = source.OrderBy(s => s.LastName);
                    break;

                case SortState.LastNameDesc:
                    source = source.OrderByDescending(s => s.LastName);
                    break;

                default:
                    source = source.OrderBy(s => s.Id);
                    break;
            }


            // пагинация
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new SortViewModel(sortOrder),
                FilterViewModel = new FilterViewModel(firstName, lastName),
                Users = items
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult UserInfo(UserInfo info)
        {
            var user = _userService.GetInfo(info.Id);
            user.FirstName = info.FirstName;
            user.LastName = info.LastName;
            _userService.Update(user);
            //return Redirect(Url.Action("GetAll", "Home"));
            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public IActionResult CreateUser(UserInfo info)
        {
            var user = new User()
            {
                FirstName = info.FirstName,
                LastName = info.LastName,
                Login = info.Login,
                Password = info.Password,
                DateOfBirth = info.DateOfBirth
            };
            var userRole = _userService.GetRoles().FirstOrDefault(r => r.Name == info.Role);
            user.Role = userRole;
            _userService.Create(user);
            return Redirect(Url.Action("GetAll", "User"));
        }

        public IActionResult CreatePage()
        {
            return View();
        }

        public IActionResult DeleteUser(UserInfo info)
        {
            var user = _userService.GetInfo(info.Id);
            _userService.Delete(user);

            return RedirectToAction("GetAll", "User");

            //return Redirect(Url.Action("GetAll", "Home"));
        }

        public IActionResult DeleteTasks(UserInfo info)
        {
            foreach (var id in info.SelectedTaskIds)
            {
                var task = _taskService.GetInfo(id);
                _taskService.Delete(task);
            }

            return RedirectToAction("GetInfo", "User", new { info.Id });
        }

    }
}
