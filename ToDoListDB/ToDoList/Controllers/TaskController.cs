﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Models;
using ToDoList.Models.Home;
using ToDoList.Models.ViewModels;
using ToDoListDB.Model;

namespace ToDoList.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        public TaskController(ITaskService taskService, IUserService userService)
        {
            _taskService = taskService;
            _userService = userService;
        }
        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var task = _taskService.GetInfo(id);
            var model = new TaskInfo
            {
                Id = task.Id,
                Title = task.Title,
                Description = task.Description
            };

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult GetAll(string title, int page = 1, SortState sortOrder = SortState.IdAsc)
        {
            int pageSize = 3;

            var login = User.Identity.Name;
            var user = _userService.GetAll().FirstOrDefault(u => u.Login == login);            
            var tasks = _taskService.GetAll(t => t.Users.Any(u => u.Id == user.Id)).AsQueryable();

            var model = tasks
                .Select(u => new TaskInfo
                {
                    Id = u.Id,
                    Title = u.Title,
                    Description = u.Description
                });
            IQueryable<TaskInfo> source = model;

            //фильтрация
            if (!String.IsNullOrEmpty(title))
            {
                source = source.Where(p => p.Title.Contains(title));
            }

            // сортировка
            switch (sortOrder)
            {
                case SortState.IdDesc:
                    source = source.OrderByDescending(s => s.Id);
                    break;

                case SortState.TitleAsc:
                    source = source.OrderBy(s => s.Title);
                    break;

                case SortState.TitleDesc:
                    source = source.OrderByDescending(s => s.Title);
                    break;

                case SortState.DescriptionAsc:
                    source = source.OrderBy(s => s.Description);
                    break;

                case SortState.DescriptionDesc:
                    source = source.OrderByDescending(s => s.Description);
                    break;

                default:
                    source = source.OrderBy(s => s.Id);
                    break;
            }


            // пагинация
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = new PageViewModel(count, page, pageSize),
                SortViewModel = new SortViewModel(sortOrder),
                FilterViewModel = new FilterViewModel(title),
                Tasks = items
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult TaskInfo(TaskInfo info)
        {
            var task = _taskService.GetInfo(info.Id);
            task.Title = info.Title;
            task.Description = info.Description;


            _taskService.Update(task);
            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public IActionResult CreateTask(TaskInfo info)
        {
            var login = User.Identity.Name;
            var user = _userService.GetAll().FirstOrDefault(u => u.Login == login);
            var task = new Task()
            {
                Title = info.Title,
                Description = info.Description,
            };
            user.Tasks.Add(task);

            _taskService.Create(task);
            return Redirect(Url.Action("GetAll", "Task"));
        }

        public IActionResult CreatePage()
        {
            return View();
        }

        public IActionResult DeleteTask(TaskInfo info)
        {
            var task = _taskService.GetInfo(info.Id);
            _taskService.Delete(task);

            return RedirectToAction("GetAll", "Task");
        }        
    }
}
