﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ToDoListDB.Model
{
    public class Task
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual Task ParentTask { get; set; }

        public string Description { get; set; }
        [Required]
        public string Title { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Task> ChildrenTasks { get; set; }
    }
}
