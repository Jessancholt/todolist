﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoListDB.Repositories.Interfaces;

namespace ToDoListDB.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        private readonly TDLDbContext _context;
        public GenericRepository(TDLDbContext context)
        {
            _context = context;
        }

        public T Read(int id)
        {
            return _context.Find<T>(id);
        }

        public IQueryable<T> ReadAll()
        {
            return _context.Set<T>();
        }

        public T Create(T user)
        {
            return _context.Add(user).Entity;
        }

        public T Update(T entity)
        {
            return _context.Update(entity).Entity;
        }

        public virtual void Delete(T entity)
        {
            _context.Remove(entity);
        }
    }
}
