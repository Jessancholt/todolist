﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListDB.Model;

namespace ToDoListDB.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        public UserRepository Users { get; }

        GenericRepository<Task> Tasks { get; }
        GenericRepository<Role> Roles { get; }

        public void SaveChanges();
    }
}
