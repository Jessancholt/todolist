﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListDB.Model;

namespace ToDoListDB.Repositories.Interfaces
{
    interface IUserRepository : IGenericRepository<User>
    {
        public void Delete(User user);
    }
}
