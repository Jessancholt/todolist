﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoListDB.Repositories.Interfaces
{
    public interface IGenericRepository<T>
        where T : class
    {
        public T Read(int id);

        public IQueryable<T> ReadAll();

        public T Create(T entity);

        public T Update(T entity);

        public void Delete(T entity);
    }
}
