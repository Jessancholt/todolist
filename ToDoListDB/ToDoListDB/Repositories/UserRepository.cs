﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListDB.Model;
using ToDoListDB.Repositories.Interfaces;

namespace ToDoListDB.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private TDLDbContext _context;

        public UserRepository(TDLDbContext context) : base(context)
        {
            _context = context;
        }

        public override void Delete(User user)
        {
            user.IsDeleted = true;
            _context.Update(user);
        }
    }
}
