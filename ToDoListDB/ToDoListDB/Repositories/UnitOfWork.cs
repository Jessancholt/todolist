﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListDB.Model;
using ToDoListDB.Repositories.Interfaces;

namespace ToDoListDB.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly TDLDbContext _context;

        public UnitOfWork(TDLDbContext context)
        {
            _context = context;
        }

        private UserRepository _users;

        public UserRepository Users => _users ??= new UserRepository(_context);

        public GenericRepository<Role> Roles => new GenericRepository<Role>(_context);
        public GenericRepository<Task> Tasks => new GenericRepository<Task>(_context);

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
