﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListDB.Model;
using ToDoListDB.Repositories.Interfaces;

namespace Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;

        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public List<User> GetAll()
        {
            return
                _uow.Users
                .ReadAll()
                .ToList();
        }

        public User GetInfo(int id)
        {
            return _uow.Users.Read(id);
        }

        public User Update(User user)
        {            
            _uow.Users.Update(user);
            _uow.SaveChanges();
            return user;
        }

        public void Create(User user)
        {
            _uow.Users.Create(user);
            _uow.SaveChanges();
        }
        public void Delete(User user)
        {
            _uow.Users.Delete(user);
            _uow.SaveChanges();
        }

        public List<Role> GetRoles()
        {
            return _uow.Roles.ReadAll().ToList();
        }
    }
}
