﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListDB.Model;

namespace Services.Interfaces
{
    public interface IUserService
    {
        public List<User> GetAll();
        public User GetInfo(int id);
        public User Update(User user);
        public void Create(User user);
        public void Delete(User user);
        public List<Role> GetRoles();
    }
}
