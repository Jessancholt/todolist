﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ToDoListDB.Model;

namespace Services.Interfaces
{
    public interface ITaskService
    {
        public List<Task> GetAll();
        public List<Task> GetAll(Expression<Func<Task, bool>> condition);
        public Task GetInfo(int id);
        public Task Update(Task task);
        public void Create(Task task);
        public void Delete(Task task);
    }
}
