﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ToDoListDB.Model;
using ToDoListDB.Repositories.Interfaces;

namespace Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _uow;

        public TaskService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public List<Task> GetAll()
        {
            return
                _uow.Tasks
                .ReadAll()
                .ToList();
        }

        //var tasklist = taskservice.GetAll(t => t.Users.Any( u=> u.Id == userId))
        public List<Task> GetAll(Expression<Func<Task, bool>> condition)
        {
           return
                _uow.Tasks
                  .ReadAll().Where(condition).ToList();
        }

        public Task GetInfo(int id)
        {
            return _uow.Tasks.Read(id);
        }

        public Task Update(Task task)
        {
            _uow.Tasks.Update(task);
            _uow.SaveChanges();
            return task;
        }

        public void Create(Task task)
        {
            _uow.Tasks.Create(task);
            _uow.SaveChanges();
        }

        public void Delete(Task task)
        {
            _uow.Tasks.Delete(task);
            _uow.SaveChanges();
        }
    }
}
