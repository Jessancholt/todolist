﻿using System;
using System.Collections.Generic;

namespace BinaryTask
{
    public class BinaryString
    {
        public static string GetGoodString(string binary)
        {
            int zeroCount = 0;
            int oneCount = 0;
            bool check = false;
            bool checkTwo = false;
            List<string> prefixes = new List<string>();
            for (int i = 1; i <= binary.Length; i++)
            {
                prefixes.Add(binary[..i]);
            }
            Cycle(binary, ref zeroCount, ref oneCount);
            if (zeroCount == oneCount)
            {
                check = true;
            }

            foreach (var prefix in prefixes)
            {
                Cycle(prefix, ref zeroCount, ref oneCount);
            }

            if (oneCount > zeroCount)
                checkTwo = true;

            if (check && checkTwo)
                return "Valid";
            else
                return "Invalid";
        }

        private static void Cycle(string binary, ref int zeroCount, ref int oneCount)
        {
            foreach (var number in binary)
            {
                if (number == '0')
                    zeroCount++;
                else
                    oneCount++;
            }
        }
    }
}
