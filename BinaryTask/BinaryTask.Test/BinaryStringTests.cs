﻿using NUnit.Framework;
using System;

namespace BinaryTask.Test
{
    [TestFixture]
    public class BinaryStringTests
    {
        [TestCase("01011", ExpectedResult = "Invalid")]
        [TestCase("11010", ExpectedResult = "Invalid")]
        [TestCase("110010", ExpectedResult = "Valid")]
        [TestCase("00011110", ExpectedResult = "Invalid")]
        public string GetGoodString(string binary)
        {
            return BinaryString.GetGoodString(binary);
        }
    }
}
